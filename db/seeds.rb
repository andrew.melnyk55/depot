# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Product.delete_all
# ...
Product.create!(title: 'Programming Ruby 2.2.3',
	description:
		%{<p>
				Ruby is the fastest growing and most exciting dymanic language
				out there. If you need to get working program delivered fast,
				you should add Ruby to your toolbox.
		 	 </p>},
	image_url: 'prod_01.png',
	price: 49.95)

Product.create!(title: 'Programming Ruby 2.2.2',
	description:
		%{<p>
				Ruby is the fastest growing and most exciting dymanic language
				out there. If you need to get working program delivered fast,
				you should add Ruby to your toolbox.
		 	 </p>},
	image_url: 'prod_02.png',
	price: 39.95)

Product.create!(title: 'Programming Ruby 2.2.1',
	description:
		%{<p>
				Ruby is the fastest growing and most exciting dymanic language
				out there. If you need to get working program delivered fast,
				you should add Ruby to your toolbox.
		 	 </p>},
	image_url: 'prod_03.png',
	price: 29.95)
# ...